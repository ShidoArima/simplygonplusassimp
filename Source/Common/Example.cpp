﻿///////////////////////////////////////////////////////////////////////////
//
//  System:    Simplygon
//  File:      Example.cpp
//  Language:  C++
// 
//  Copyright (c) 2015 Donya Labs AB. All rights reserved.
//
///////////////////////////////////////////////////////////////////////////
//
//	#Description# Example.h & Example.cpp implements common example 
//	functions, such as init/deinit and error handling, which are used by 
//	all examples in the Simplygon SDK.
//
///////////////////////////////////////////////////////////////////////////

#include "Example.h"
#include <fstream>
#include <streambuf>
#include <string>
#include <time.h>


ISimplygonSDK *sg = NULL;

static time_t start_time;
static time_t end_time;
static time_t delta_time;

// Error handling class
class error_handler : public SimplygonSDK::rerrorhandler
	{
	public:
		virtual void HandleError( 
						IObject *object , 
						const char *interfacename , 
						const char *methodname , 
						rid errortype , 
						const char *errortext 
						)
			{
			printf("An error occured!\n");
			printf("\tInterface: %s (%p)\n",interfacename,object);
			printf("\tMethod: %s\n",methodname);
			printf("\tError Type: %d\n",errortype);
			printf("\tError Description: %s\n",errortext);

			fprintf( stderr , "A SimplygonSDK error occured!\n");
			fprintf( stderr , "\tInterface: %s (%p)\n",interfacename,object);
			fprintf( stderr , "\tMethod: %s\n",methodname);
			fprintf( stderr , "\tError Type: %d\n",errortype);
			fprintf( stderr , "\tError Description: %s\n",errortext);

			exit(1);
			}
	} eh;

void ExitWithError( const std::string& errorstr )
	{
	fprintf( stderr , "Runtime error: %s\n", errorstr.c_str() );
	exit(1);
	}

void InitExample()
	{
	start_time = time(nullptr);

	// Initiate
#ifdef _WIN32
	std::string path = GetExecutablePath() + "Core\\";
	SimplygonSDK::AddSearchPath(path.c_str());

	int initval = SimplygonSDK::Initialize( &sg );
#elif defined(__linux__)
	std::ifstream t("LicensePathHere");
	std::string licenceStr((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
	int initval = SimplygonSDK::Initialize( &sg, "./SimplygonSDKNixRuntime.so", licenceStr.c_str() );
#elif defined(__APPLE__)
	std::ifstream t("LicensePathHere");
	std::string licenceStr((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
	int initval = SimplygonSDK::Initialize( &sg, "./SimplygonSDKNixRuntime.dylib", licenceStr.c_str() );
#endif

	if( initval != SimplygonSDK::SG_ERROR_NOERROR )
		{
		fprintf(stderr,"Failed to initialize.\n\tError: %d\n\tError string:%s\n", initval, SimplygonSDK::GetError(initval) );
		exit(1);
		}
	sg->SetErrorHandler(&eh);

	// Set working directory to executable directory
#ifdef _WIN32
	SetCurrentDirectory(GetExecutablePath().c_str());
#elif defined(__linux__) || defined(__APPLE__)
	chdir(GetExecutablePath().c_str());
#endif
	}

void DeinitExample()
	{
	// Deinitialize the SDK
	SimplygonSDK::Deinitialize();

	end_time = time(nullptr);
	delta_time = end_time - start_time;

	printf("\nProcessing took %lld seconds.\n", delta_time);
	}

std::string GetExecutablePath()
	{
#ifdef _WIN32

	char currentDir[MAX_PATH];
	char drive[MAX_PATH];
	char dir[MAX_PATH];
	char filename[MAX_PATH];
	char ext[MAX_PATH];

	if( GetModuleFileName( NULL , currentDir , MAX_PATH ) == NULL )
		{
		return std::string("");
		}

	_splitpath_s( currentDir, drive, _MAX_DRIVE, dir, _MAX_DIR, filename, _MAX_FNAME, ext, _MAX_EXT );
	_makepath_s( currentDir, _MAX_PATH, drive, dir, "", "" );

	return std::string(currentDir);

#elif defined(__linux__)

	char currentDir[MAX_PATH];
	ssize_t len = readlink("/proc/self/exe", currentDir, MAX_PATH);
	currentDir[len] = '\0';

	std::string path = currentDir;
	path = path.substr(0, path.find_last_of("/\\")) + "/";
	return path;

#elif defined(__APPLE__)

	char currentDir[MAX_PATH];
	uint32_t bufsize = MAX_PATH;
	if( _NSGetExecutablePath(currentDir, &bufsize) == -1 )
		{
		return std::string("");
		}
	return std::string(currentDir);

#endif
}

std::string GetAssetPath()
	{
	std::string assetPath;
	std::string currentWorkingDirectory = GetExecutablePath();
	std::vector<std::string> folders = StringSplit(currentWorkingDirectory.c_str(), _T("\\/") );

	// Look for Build folder, Asset folder should be next to it
	std::vector<std::string>::iterator it;
	for ( it=folders.begin() ; it < folders.end(); it++ )
		{
		if(*it == _T("Build") )
			{
			break;
			}
		assetPath += *it;
		assetPath += "/";
		}

	assetPath+=_T("Assets/");
#if defined(__linux__) || defined(__APPLE__)
	assetPath = "/" + assetPath;
#endif
	return assetPath;
	}


std::vector<std::string> StringSplit(const std::string& source, const std::string& delim)
	{

	std::stringstream stringStream(source);
	std::string line;
	std::vector<std::string> stringCollection;

	while(std::getline(stringStream, line)) 
		{
		std::size_t prev = 0;
		std::size_t pos;
		while ((pos = line.find_first_of(delim, prev)) != std::string::npos)
			{
			if (pos > prev)
				{
				stringCollection.push_back(line.substr(prev, pos-prev));
				}
			prev = pos+1;
			}
		if (prev < line.length())
			{
			stringCollection.push_back(line.substr(prev, std::string::npos));
			}
		}
	return stringCollection;
	}

void AddSimplygonTexture( spMaterial mat, spTextureTable texTable, const char * channelName, const char * texFilePath, const char* namePrefix )
	{
	char texName[256];
	sprintf_s( texName, "%s%s", namePrefix, channelName );

	spTexture newTex = sg->CreateTexture();
	newTex->SetFilePath( texFilePath );
	newTex->SetName( texName );
	texTable->AddTexture( newTex );
	spShadingTextureNode texNode = sg->CreateShadingTextureNode();
	texNode->SetTextureName( texName );
	texNode->SetTextureLevel( 0 );
	mat->SetShadingNetwork( channelName, texNode );
	}
