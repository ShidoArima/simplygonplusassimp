﻿///////////////////////////////////////////////////////////////////////////
//
//  System:    Simplygon
//  File:      ReductionExample.cpp
//  Language:  C++
//
//  Copyright (c) 2015 Donya Labs AB. All rights reserved.
//
//  This is private property, and it is illegal to copy or distribute in
//  any form, without written authorization by the copyright owner(s).
//
///////////////////////////////////////////////////////////////////////////
//
//	#Description# An extensive reduction example
//
//	In this example we demonstrate 3 common usages for the Simplygon reducer
//	and explain good starting settings for the different usages. We also discuss
//	how to use the stop conditions to setup complex reduction targets.
//
//	First, we do a high-quality reduction using symmetry awareness and 
//	high-quality normal handling, explaining most of the commonly 
//	used settings along the way
//
//	Secondly, the mesh is again reduced and the 9 input materials
//	of the original are cast into a single output material, retaining all 
//	channels from the original material
//
//	Thirdly, a cascaded reduction chain is run using faster settings
//	to show how to setup a cascaded LOD chain.
//
///////////////////////////////////////////////////////////////////////////

#include <boost/filesystem.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "../AssimpImporter.h"
#include "../Common/Example.h"
#include <Magick++.h>

using namespace std;
using namespace boost;
using namespace boost::program_options;

namespace fs = boost::filesystem;

string ToLower(string& data);
void DirectoryProcessReduce(fs::path parent, int reduce, bool searchChild);
spGeometryData GetFirstGeometry(spSceneNode node, std::string space);
void RunReduce(fs::path readFrom, const std::string& writeTo, int reduceSize);
void RunReductionWithTextureCasting( const std::string& readFrom, const std::string& writeTo, int reduceSize, IGraphicsImporter& importer);

const std::string FOLDER_NAME = "Result Models";

class ProgressObserver : public robserver
	{
		public:
		virtual void Execute(
			IObject * subject,
			rid EventId,
			void * EventParameterBlock,
			unsigned int EventParameterBlockSize )
			{
			// only care for progress events
			if( EventId == SG_EVENT_PROGRESS )
				{
				// get the progress in percent
				int val = *((int *)EventParameterBlock);
				// tell the process to continue
				// this is required by the progress event
				*((int *)EventParameterBlock) = 1;
				// output the progress update
				PrintProgressBar( val );
				}
			}
	} progressObserver;

int main( int argc, char **argv)
	{
	Magick::InitializeMagick(*argv);
	InitExample();

	options_description desc("\n Allowed arguments");
	desc.add_options()
		("help,h", "Produce this help message.")
		("input - file,i", value<vector<string> >(), "[string] Specifies input file or folder")
		("reduce,r", value<vector<int> >(), "[int] Specifies reduce percent")
		("children,c", "Include children.");

	variables_map vm;

	try
	{
		store(command_line_parser(argc, argv).options(desc).run(), vm);
		notify(vm);
	}
	catch (std::exception &e)
	{
		cout << endl << e.what() << endl;
		cout << desc << endl;
	}

	string folder = GetExecutablePath() + FOLDER_NAME + "\\";
	CreateDirectory(folder.c_str(), NULL);

	sg->SetGlobalSetting( "DefaultTBNType", SG_TANGENTSPACEMETHOD_ORTHONORMAL );

	string assetPath;
	int reduce = 50;
	bool searchChild = false;

	cout << "Add argument -h for help \n" << endl;

	if (vm.count("help"))
	{
		cout << desc << endl;\
	}

	if (vm.count("input - file"))
	{
		vector<string> inputFilename =vm["input - file"].as<vector<string>>();
		assetPath = inputFilename[0];
	}
	else
	{
		cout << "Input file not specified" << endl;
		return -1;
	}
	
	if (!fs::exists(assetPath))
	{
		printf("File or directory not exist. \n");
		return -1;
	}

	fs::path path = fs::path(assetPath);

	if (vm.count("reduce"))
	{
		vector<int> inputFilename = vm["reduce"].as<vector<int>>();
		reduce = inputFilename[0];
		reduce = max(1, min(reduce, 100));
	}

	if (vm.count("children"))
	{
		searchChild = true;
	}

	if (fs::is_regular_file(path))
	{			
		RunReduce(path, path.stem().string(), reduce);
	}
	else if(fs::is_directory(path))
	{
		DirectoryProcessReduce(path, reduce, searchChild);
	}

	// Done!
	printf( "All LODs complete, shutting down..." );
	DeinitExample();

	cin.get();

	return 0;
	}

void DirectoryProcessReduce(fs::path parent, int reduce, bool searchChild)
{
	fs::directory_iterator end_iter;
	for (fs::directory_iterator dir_itr(parent);  dir_itr != end_iter; ++dir_itr)
	{
		try
		{
			if (fs::is_regular_file(dir_itr->status()))
			{
				RunReduce(dir_itr->path(), dir_itr->path().stem().string(), reduce);
			}
			else if (fs::is_directory(dir_itr->status()) && searchChild)
			{
				DirectoryProcessReduce(dir_itr->path(), reduce, searchChild);
			}
		}
			catch (const std::exception & ex)
		{
			cout << dir_itr->path().filename() << " " << ex.what() << endl;
		}
	}
}

string ToLower(string& data)
{
	transform(data.begin(), data.end(), data.begin(), (int(*)(int))tolower);
	return data;
}

void RunReduce(fs::path readFrom, const std::string& writeTo, int reduceSize)
{
	std::string ext = readFrom.extension().string();

	spWavefrontImporter objImport = sg->CreateWavefrontImporter();
	AssimpImporter* assimpImporter = new AssimpImporter(sg);

	IGraphicsImporter* importer;
	if (ToLower(ext) == ".obj")
	{
		importer = objImport;
	}
	else
	{
		importer = assimpImporter;
	}

	std::string format = "Reduce " + writeTo + " by " + std::to_string(reduceSize) + (std::string)"% ... \n";
	printf(format.c_str());

	//PrintProgressBar(0);

	RunReductionWithTextureCasting(readFrom.string(), writeTo, reduceSize, *importer);
	printf("\nDone.\n\n");
}

void RunReductionWithTextureCasting( const std::string& readFrom, const std::string& writeTo, int reduceSize, IGraphicsImporter& importer)
	{
		importer.SetImportFilePath( readFrom.c_str() );
		if( !importer.RunImport() )
			return;

		std::string folder = GetExecutablePath() + FOLDER_NAME + "\\" + writeTo + "\\";
		// Generate the output filenames
		CreateDirectory(folder.c_str(), NULL);

		// Get the scene from the importer
		spScene originalScene = importer.GetScene();

#if _DEBUG
		{
			cout << endl;
			GetFirstGeometry(originalScene->GetRootNode(), "- ");
			spTextureTable lodTexTable = originalScene->GetTextureTable();
			spImageDataExporter exporter = sg->CreateImageDataExporter();

			for (int i = 0; i < lodTexTable->GetTexturesCount(); i++)
			{
				spTexture tex = lodTexTable->GetTexture(i);
				string path = folder.c_str(); 
				path.append(tex->GetName());
				exporter->SetExportFilePath(path.c_str());
				exporter->SetImage(tex->GetImageData());
				if (!exporter->RunExport())
					cout << "Export error: " + path << endl;

			}

			spWavefrontExporter objExporter = sg->CreateWavefrontExporter();
			std::string output_geometry_filename = folder + writeTo + "_Imported.obj";

			// Do the actual exporting
			objExporter->SetExportFilePath(output_geometry_filename.c_str());
			objExporter->SetScene(originalScene);
			objExporter->RunExport();
		}
#endif
		spScene aggregatedScene = sg->CreateScene();
		aggregatedScene->DeepCopy(originalScene);

		///////////////////////////////////////////////////////////////////////////////////////////////
		// AGGREGATION
		///////////////////////////////////////////////////////////////////////////////////////////////

		// Create the spAggregationProcessor
		spAggregationProcessor aggregationProcessor = sg->CreateAggregationProcessor();
		// Set the input scene
		aggregationProcessor->SetScene(aggregatedScene);

		spAggregationSettings aggregatorSettings = aggregationProcessor->GetAggregationSettings();

		// Set the BaseAtlasOnOriginalTexCoords to true so that the new texture
		// coords will be based on the original. This way the four identical
		// SimplygonMan mesh instances will still share texture coords, and
		// when packing the texture charts into the new atlas, only rotations
		// multiples of 90 degrees are allowed.
		aggregatorSettings->SetBaseAtlasOnOriginalTexCoords(true);

		// Get the mapping image settings, a mapping image is needed to
		// cast the new textures.
		spMappingImageSettings mappingImageSettings = aggregationProcessor->GetMappingImageSettings();
		mappingImageSettings->SetGenerateMappingImage(true);
		mappingImageSettings->SetGutterSpace(1);
		mappingImageSettings->SetWidth(2048);
		mappingImageSettings->SetHeight(2048);
		mappingImageSettings->SetUseFullRetexturing(true); //replace old UVs

		// If BaseAtlasOnOriginalTexCoords is enabled and 
		// if charts are overlapping in the original texture coords, they will be separated if
		// SeparateOverlappingCharts is set to true.
		mappingImageSettings->SetChartAggregatorSeparateOverlappingCharts(false);

		//Add observer
		aggregationProcessor->AddObserver(&progressObserver, SG_EVENT_PROGRESS);
		// Run the process
		aggregationProcessor->RunProcessing();

		//Clear the copied materials from the lod, and later populate it with newly generated materials
		spTextureTable lodTexTable = aggregatedScene->GetTextureTable();
		lodTexTable->Clear();
		spMaterialTable lodMatTable = aggregatedScene->GetMaterialTable();
		lodMatTable->Clear();

		//Create a new material that we cast the new textures onto
		spMaterial lodMaterial = sg->CreateMaterial();
		lodMaterial->SetName("output_material");

		lodMatTable->AddMaterial(lodMaterial);
		lodMatTable->SetName("CombinedMaterials");

		spColorCaster diffuseCaster = sg->CreateColorCaster();
		diffuseCaster->SetColorType(SG_MATERIAL_CHANNEL_DIFFUSE);
		diffuseCaster->SetSourceMaterials(originalScene->GetMaterialTable());
		diffuseCaster->SetSourceTextures(originalScene->GetTextureTable());

		std::string diffusePath = folder + "outputDiffuse.png";
		diffuseCaster->SetOutputFilePath(diffusePath.c_str());
		diffuseCaster->SetMappingImage(aggregationProcessor->GetMappingImage());

		diffuseCaster->RunProcessing();

		//Setup new texture in LOD scene
		AddSimplygonTexture(lodMaterial, lodTexTable, SG_MATERIAL_CHANNEL_DIFFUSE, "outputDiffuse.png");

		spColorCaster specularCaster = sg->CreateColorCaster();
		specularCaster->SetColorType(SG_MATERIAL_CHANNEL_SPECULAR);
		specularCaster->SetSourceMaterials(originalScene->GetMaterialTable());
		specularCaster->SetSourceTextures(originalScene->GetTextureTable());

		std::string specularPath = folder + "outputSpecular.png";
		specularCaster->SetOutputFilePath(specularPath.c_str());
		specularCaster->SetMappingImage(aggregationProcessor->GetMappingImage());

		specularCaster->RunProcessing();

		//Setup new texture in LOD scene
		AddSimplygonTexture(lodMaterial, lodTexTable, SG_MATERIAL_CHANNEL_SPECULAR, "outputSpecular.png");

		// cast the data using a normal caster
		spNormalCaster normalCaster = sg->CreateNormalCaster();
		normalCaster->SetSourceMaterials(originalScene->GetMaterialTable());
		normalCaster->SetSourceTextures(originalScene->GetTextureTable());
		normalCaster->SetMappingImage(aggregationProcessor->GetMappingImage());
		normalCaster->SetOutputChannels(3); // RGB, 3 channels! (But really the x, y and z values for the normal)
		normalCaster->SetOutputChannelBitDepth(8);
		normalCaster->SetDilation(10);

		std::string normalPath = folder + "outputNormalMap.png";
		normalCaster->SetOutputFilePath(normalPath.c_str());
		normalCaster->SetFlipBackfacingNormals(false);
		normalCaster->SetGenerateTangentSpaceNormals(true);

		normalCaster->RunProcessing();

		// Set normal map of the created material to point to the combined normal map
		AddSimplygonTexture(lodMaterial, lodTexTable, SG_MATERIAL_CHANNEL_NORMALS, "outputNormalMap.png");
		{
			// Store to file
			spWavefrontExporter objExporter = sg->CreateWavefrontExporter();

			std::string output_geometry_filename = folder + writeTo + "_Aggregated.obj";
			objExporter->SetExportFilePath(output_geometry_filename.c_str());
			spGeometryData geometry = ISceneMesh::SafeCast(aggregatedScene->GetRootNode()->GetChild(0))->GetGeometry();
		
			//Sets all triangle's material IDs to the single new material
			for (uint i = 0; i < geometry->GetTriangleCount(); ++i)
			{
				geometry->GetMaterialIds()->SetItem(i, 0);
			}

			objExporter->SetScene(aggregatedScene);
			objExporter->RunExport();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// REDUCTION
		///////////////////////////////////////////////////////////////////////////////////////////////

		//Create a new Scene based on the aggregated scene.
		spScene reducedScene = sg->CreateScene();
		reducedScene->DeepCopy(aggregatedScene);

		// Create the reduction-processor, and set the scene to reduce
		spReductionProcessor reductionProcessor = sg->CreateReductionProcessor();
		reductionProcessor->SetScene(reducedScene);

		///////////////////////////////////////////////////////////////////////////////////////////////
		// SETTINGS - Most of these are set to the same value by default, but are set anyway for clarity

		// The reduction settings object contains settings pertaining to the actual decimation
		spReductionSettings reductionSettings = reductionProcessor->GetReductionSettings();
		reductionSettings->SetReductionHeuristics( SG_REDUCTIONHEURISTICS_FAST ); //Choose between "fast" and "consistent" processing.

		// The actual reduction triangle target are controlled by these three settings
		reductionSettings->SetStopCondition( SG_STOPCONDITION_ANY );//The reduction stops when any of the targets below is reached
		reductionSettings->SetReductionTargets( SG_REDUCTIONTARGET_ALL );//Selects which targets should be considered when reducing
		reductionSettings->SetTriangleRatio((float)reduceSize / 100); //Targets at 50% of the original triangle count
		reductionSettings->SetTriangleCount( 10 ); //Targets when only 10 triangle remains
		reductionSettings->SetMaxDeviation( REAL_MAX ); //Targets when an error of the specified size has been reached. As set here it never happens.
		reductionSettings->SetOnScreenSize( 50 ); //Targets when the LOD is optimized for the selected on screen pixel size

		// The normal calculation settings deal with the normal-specific reduction settings
		spNormalCalculationSettings normalSettings = reductionProcessor->GetNormalCalculationSettings();
		normalSettings->SetReplaceNormals( true ); //If true, this will turn off normal handling in the reducer and recalculate them all afterwards instead.
		normalSettings->SetHardEdgeAngleInRadians( 3.14f ); //If the normals are recalculated, this sets the hard-edge angle. Large here, since we're baking a new normal map, and want large charts.
		// Add progress observer
		reductionProcessor->AddObserver( &progressObserver, SG_EVENT_PROGRESS );

		// Run the actual processing. After this, the set geometry will have been reduced according to the settings
		reductionProcessor->RunProcessing();

		{
			spWavefrontExporter objExporter = sg->CreateWavefrontExporter();
			std::string output_geometry_filename = folder + writeTo + "_Reduced.obj";

			// Do the actual exporting
			objExporter->SetExportFilePath(output_geometry_filename.c_str());
			objExporter->SetScene(reducedScene); //This is the geometry we set as the processing geom of the reducer
			objExporter->RunExport();
		}

		//Done! LOD and material created.
	}

	void LogHierarchy(spSceneNode node, std::string space)
	{
		cout << space;
		cout << node->GetName() << endl;

		for (int i = 0; i < node->GetChildCount(); i++)
		{
			LogHierarchy(node->GetChild(i), space + "- ");
		}
	}
