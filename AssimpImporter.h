#ifndef ASSIMP_IMPORTER_H
#define ASSIMP_IMPORTER_H

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <vector>
#include <float.h>
#include <Matrix4x4.h>

#ifdef _WIN32
#include <SimplygonSDKLoader.h>
#include <windows.h>
#include <process.h>

#elif defined(__linux__) || defined(__APPLE__)

#include <SimplygonSDKNixLoader.h>
#include <pthread.h>
#include <unistd.h>

#ifndef MAX_PATH
#if defined(__linux__)
#include <linux/limits.h>
#elif defined(__APPLE__)
#include <sys/syslimits.h>
#endif
#define MAX_PATH PATH_MAX
#endif

#if defined(__APPLE__)
#include <mach-o/dyld.h>
#endif
#endif

namespace SimplygonSDK
{
	class AssimpImporter : public IGraphicsImporter
	{		
	public:

		struct MaterialMap
		{
			int Ai_Id;
			int Sg_Id;

			MaterialMap(int& aiId, int& sgId)
			{
				Ai_Id = aiId;
				Sg_Id = sgId;
			}
		};

		AssimpImporter(ISimplygonSDK* sg);

		AssimpImporter(const char* value);

		CountedPointer<IScene> GetScene();

		void Clear();

		rstring GetImportFilePath();

		void SetImportFilePath(const char* value);

		bool RunImport();

		void PrintInfo();

		void AddRef();

		void Release();

		void SetName(const rchar * Name);

		rstring GetName();

		rid AddObserver(robserver * Observer, rid EventId);

		void RemoveObserver(rid ObserverId);

		void * GetUserComponentArea(rid userid);

		const rchar *GetClass();

		bool IsA(const rchar *type) const;
	};
}

#endif