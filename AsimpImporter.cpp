#include "AssimpImporter.h"
#include <assimp/Importer.hpp> // C++ importer interface
#include <assimp/scene.h> // Output data structure
#include <assimp/postprocess.h> // Post processing flags
#include <Magick++.h>

using namespace std;
using namespace Assimp;

namespace SimplygonSDK
{
	void ReadScene();
	void RecursiveConvert(const struct aiNode* nd, spSceneMesh node, spMatrix4x4 parentTransform);
	spGeometryData ConvertMesh(const aiMesh* mesh);
	SimplygonSDK::spMatrix4x4 ContertMatrix(aiMatrix4x4 m);
	int GetMaterialId(int aiMatId);
	void LoadMaterials(const aiScene* scene);
	rid CreateMaterial(int mId, const aiMaterial *mtl);
	void ResolveTexture(const char* texPath);
	bool EmbeddedTextureHandler(const char* texPath);
	bool ExternalTextureHandler(const char* texPath);
	spImageData CreateTexture(const aiTexture* texture);
	spImageData CreateTexture(Magick::Image* texture);
	spShadingNode GenerateShadingNode(const aiMaterial *mtl, const char *key, unsigned int type, unsigned int index, aiTextureType texType);

	string _path;

	ISimplygonSDK* _sg;
	Importer _importer;
	spMaterialTable _matTable;
	spTextureTable _texTable;

	spScene _sgScene;
	const aiScene* _aiScene;

	vector<AssimpImporter::MaterialMap> _aiToSgMap;

	AssimpImporter::AssimpImporter(ISimplygonSDK* simplygon)
	{
		_sg = simplygon;
		_aiToSgMap = vector<AssimpImporter::MaterialMap>();
	}

	AssimpImporter::AssimpImporter(const char* value)
	{
		SetImportFilePath(value);
	}

	CountedPointer<IScene> AssimpImporter::GetScene()
	{
		return _sgScene;
	}

	void AssimpImporter::Clear()
	{
	}

	rstring AssimpImporter::GetImportFilePath()
	{
		return rstring();
	}
	void AssimpImporter::SetImportFilePath(const char* value)
	{
		_path = string(value);
	}

	bool AssimpImporter::RunImport()
	{
		_importer.SetPropertyInteger(AI_CONFIG_PP_RVC_FLAGS, 
			aiComponent_COLORS | 
			aiComponent_CAMERAS|
			aiComponent_LIGHTS |
			aiComponent_BONEWEIGHTS|
			aiComponent_ANIMATIONS);

		// And have it read the given file with some example postprocessing
		// Usually - if speed is not the most important aspect for you - you'll
		// probably to request more postprocessing than we do in this example.
		_aiScene = _importer.ReadFile(_path,
			/*aiProcess_ConvertToLeftHanded |*/
			aiProcess_CalcTangentSpace |
			aiProcess_Triangulate |
			aiProcess_SortByPType|
			aiProcess_RemoveComponent);

		// If the import failed, report it
		if (!_aiScene)
		{
			cout << _importer.GetErrorString() << endl;
			return false;
		}

		ReadScene();
		_aiScene->~aiScene();

		// We're done. Everything will be cleaned up by the importer destructor
		return true;
	}

	void ReadScene()
	{
		_sgScene = _sg->CreateScene();

		//get material table from the scene
		_matTable = _sgScene->GetMaterialTable();
		_texTable = _sgScene->GetTextureTable();

		LoadMaterials(_aiScene);

		spSceneMesh mesh = _sg->CreateSceneMesh();
		mesh->SetName("RootMesh");
		_sgScene->GetRootNode()->AddChild(mesh);

		RecursiveConvert(_aiScene->mRootNode, mesh, mesh->GetRelativeTransform());
	}

	void RecursiveConvert(const struct aiNode* nd, spSceneMesh node, spMatrix4x4 parentTransform)
	{
		aiMatrix4x4 m = nd->mTransformation;
		//create a transform node
		spTransform3 xform = _sg->CreateTransform3();
		xform->AddTransformation(ContertMatrix(m));
		xform->AddTransformation(parentTransform);

		// draw all meshes assigned to this node
		for (int i = 0; i < nd ->mNumMeshes; i++)
		{
			const struct aiMesh* mesh = _aiScene->mMeshes[nd->mMeshes[i]];

			//create two scene mesh objects
			spSceneMesh sgMesh = _sg->CreateSceneMesh();
			sgMesh->GetRelativeTransform()->DeepCopy(xform->GetMatrix());
			spGeometryData geom = ConvertMesh(mesh);

			string name = nd->mName.C_Str() + string("_Mesh_") + to_string(i);
			sgMesh->SetName(name.c_str());
			sgMesh->SetGeometry(geom);

			node->AddChild(sgMesh);
		}

		// draw all children
		for (int i = 0; i < nd->mNumChildren; i++)
		{
			RecursiveConvert(nd->mChildren[i], node, xform->GetMatrix());
		}
	}

	spGeometryData ConvertMesh(const aiMesh* mesh)
	{
		spGeometryData geom = _sg->CreateGeometryData();
		geom->SetName(mesh->mName.C_Str());
		geom->AddMaterialIds();
		geom->AddNormals();
		geom->AddTexCoords(0);

		spRealArray coords = geom->GetCoords();
		spRidArray vertex_ids = geom->GetVertexIds();
		spRealArray normals = geom->GetNormals();
		spRidArray matIds = geom->GetMaterialIds();
		spRealArray texCoords = geom->GetTexCoords(0);

		int vertex_count = mesh->mNumVertices;
		int triangle_count = mesh->mNumFaces;

		geom->SetVertexCount(vertex_count);
		geom->SetTriangleCount(triangle_count);


		for (int i = 0; i < vertex_count; i++)
		{
			aiVector3D coord = mesh->mVertices[i];
			const real vert[3] = { coord.x , coord.y, coord.z };
			coords->SetTuple(i, vert);

			if (mesh->mNormals != NULL)
			{
				coord = mesh->mNormals[i];
				const real norm[3] = { coord.x , coord.y, coord.z };
				normals->SetTuple(i, norm);

				if (mesh->HasTextureCoords(0))
				{
					coord = mesh->mTextureCoords[0][i];
					const real tex[3] = { coord.x , coord.y, coord.z };
					texCoords->SetTuple(i, tex);
				}
			}

			vertex_ids->SetItem(i, i);
		}

		//loop through all the triangles an assign material
		for (int i = 0; i < triangle_count; ++i)
		{
			matIds->SetItem(i, GetMaterialId(mesh->mMaterialIndex));
		}

		return geom;
	}

	SimplygonSDK::spMatrix4x4 ContertMatrix(aiMatrix4x4 m)
	{
		spMatrix4x4 sgM = _sg->CreateMatrix4x4();

		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				sgM->SetElement(i, j, m[i][j]);
			}
		}

		return sgM;
	}

	int GetMaterialId(int aiMatId)
	{
		for (int i = 0; i < _aiToSgMap.size(); i++)
		{
			if (aiMatId == _aiToSgMap[i].Ai_Id)
				return _aiToSgMap[i].Sg_Id;
		}

		return 0;
	}

	void LoadMaterials(const aiScene* scene)
	{
		/* getTexture Filenames and Numb of Textures */
		for (unsigned int m = 0; m<scene->mNumMaterials; m++)
		{
			CreateMaterial(m, scene->mMaterials[m]);
		}
	}

	rid CreateMaterial(int mId, const aiMaterial *mtl)
	{
		aiColor4D diffuse;
		aiColor4D specular;
		aiColor4D ambient;
		aiColor4D emission;

		int texIndex = 0;
		aiString texPath;
		aiString matName;

		spMaterial mat = _sg->CreateMaterial();
		if (AI_SUCCESS == aiGetMaterialString(mtl, AI_MATKEY_NAME, &matName))
		{
			mat->SetName(matName.C_Str());
		}

		spShadingNode diffuseNode = GenerateShadingNode(mtl, AI_MATKEY_COLOR_DIFFUSE, aiTextureType_DIFFUSE);
		mat->SetShadingNetwork(SG_MATERIAL_CHANNEL_DIFFUSE, diffuseNode);

		spShadingNode specularNode = GenerateShadingNode(mtl, AI_MATKEY_COLOR_SPECULAR, aiTextureType_SPECULAR);
		mat->SetShadingNetwork(SG_MATERIAL_CHANNEL_SPECULAR, specularNode);

		spShadingNode ambientNode = GenerateShadingNode(mtl, AI_MATKEY_COLOR_AMBIENT, aiTextureType_AMBIENT);
		mat->SetShadingNetwork(SG_MATERIAL_CHANNEL_AMBIENT, ambientNode);

		spShadingNode emissiveNode = GenerateShadingNode(mtl, AI_MATKEY_COLOR_EMISSIVE, aiTextureType_EMISSIVE);
		mat->SetShadingNetwork(SG_MATERIAL_CHANNEL_EMISSIVE, emissiveNode);

		if (AI_SUCCESS == mtl->GetTexture(aiTextureType_NORMALS, 0, &texPath))
		{
			ResolveTexture(texPath.C_Str());
			spShadingTextureNode normalNode = _sg->CreateShadingTextureNode();
			normalNode->SetName(texPath.C_Str());
			normalNode->SetTextureLevelName("Default");
			normalNode->SetTextureLevel(0);
			normalNode->SetTextureName(texPath.C_Str());
			mat->SetShadingNetwork(SG_MATERIAL_CHANNEL_NORMALS, normalNode);
		}

		rid mat_id = _matTable->AddMaterial(mat);

		_aiToSgMap.push_back(AssimpImporter::MaterialMap(mId, mat_id));

		return mat_id;
	}

	spShadingNode GenerateShadingNode(const aiMaterial *mtl, const char *key, unsigned int type, unsigned int index, aiTextureType texType)
	{
		aiColor4D color = aiColor4D(1,1,1,1);
		aiString texPath;//contains filename of texture
		bool hasTexture = false;

		if (AI_SUCCESS == mtl->GetTexture(texType, 0, &texPath))
		{
			ResolveTexture(texPath.C_Str());
			hasTexture = true;
		}

		aiGetMaterialColor(mtl, key, type, index, &color);
		spShadingColorNode colorNode = _sg->CreateShadingColorNode();
		colorNode->SetColor(color.r, color.g, color.b, color.a);

		if (hasTexture)
		{
			spShadingTextureNode textureNode = _sg->CreateShadingTextureNode();
			textureNode->SetName(texPath.C_Str());
			textureNode->SetTextureLevelName("Default");
			textureNode->SetTextureLevel(0);
			textureNode->SetTextureName(texPath.C_Str());

			spShadingMultiplyNode addNode = _sg->CreateShadingMultiplyNode();
			addNode->SetInput(0, textureNode);
			addNode->SetInput(1, colorNode);
			return addNode;
		}
		else
		{
			return colorNode;
		}
	}

	void ResolveTexture(const char* texPath)
	{
		//Embedded texture handle
		if (EmbeddedTextureHandler(texPath))
			return;

		//External texture handle
		ExternalTextureHandler(texPath);
	}

	bool EmbeddedTextureHandler(const char* texPath)
	{
		for (int i = 0; i < _aiScene->mNumTextures; i++)
		{
			const aiTexture* tex = _aiScene->mTextures[i];
			if (string(tex->mFilename.C_Str()).find(texPath) != string::npos)
			{
				spImageData img;
				if (tex->mHeight == 0)
				{
					Magick::Blob blob(tex->pcData, tex->mWidth);
					Magick::Image magickImg(blob);
					img = CreateTexture(&magickImg);
				}
				else
				{
					img = CreateTexture(tex);
				}

				spTexture texture = _sg->CreateTexture();
				texture->SetImageData(img);
				texture->SetName(tex->mFilename.C_Str());
				_texTable->AddTexture(texture);
				return true;
			}
		}

		return false;
	}

	bool ExternalTextureHandler(const char* texPath)
	{
		string directory;

		const size_t last_slash_idx = _path.rfind('\\');
		if (std::string::npos != last_slash_idx)
			directory = _path.substr(0, last_slash_idx);

		string texFullPath = directory + "\\" + texPath;

		try
		{
			Magick::Image magickImg(texFullPath);

			spImageData img = CreateTexture(&magickImg);
			img->SetName(texPath);

			spTexture texture = _sg->CreateTexture();
			texture->SetFilePath(texPath);
			texture->SetName(texPath);
			texture->SetImageData(img);
			_texTable->AddTexture(texture);
		}
		catch (exception e)
		{
			cout << "Read textrure error: ";
			cout << e.what() << endl;
			return false;
		}

		return true;
	}

	spImageData CreateTexture(const aiTexture* texture)
	{
		spImageData img = _sg->CreateImageData();

		int width = texture->mWidth;
		int height = texture->mHeight;

		img->Set2DSize(width, height);
		img->AddColors(TYPES_ID_UCHAR, "RGBA");

		spUnsignedCharArray colors = SafeCast<IUnsignedCharArray>(img->GetColors());

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				// One dimensional index
				int i = width * y + x;

				aiTexel tex = texture->pcData[i];
				// Colors to array
				colors->SetItem(i * 4 + 0, tex.r);
				colors->SetItem(i * 4 + 1, tex.g);
				colors->SetItem(i * 4 + 2, tex.b);
				colors->SetItem(i * 4 + 3, tex.a);
			}
		}
		return img;
	}

	spImageData CreateTexture(Magick::Image* texture)
	{
		spImageData img = _sg->CreateImageData();

		int width = texture->columns();
		int height = texture->rows();

		img->Set2DSize(width, height);
		img->AddColors(TYPES_ID_UCHAR, "RGBA");

		spUnsignedCharArray colors = SafeCast<IUnsignedCharArray>(img->GetColors());

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				// One dimensional index
				int i = width * y + x;

				Magick::Color pixel = texture->pixelColor(x, height-y);

				// Colors to array
				colors->SetItem(i * 4 + 0, pixel.quantumRed());
				colors->SetItem(i * 4 + 1, pixel.quantumGreen());
				colors->SetItem(i * 4 + 2, pixel.quantumBlue());
				colors->SetItem(i * 4 + 3, pixel.quantumAlpha());
			}
		}
		return img;
	}

	void AssimpImporter::PrintInfo()
	{
	}
	void AssimpImporter::AddRef()
	{
	}
	void AssimpImporter::Release()
	{
	}
	void AssimpImporter::SetName(const rchar * Name)
	{
	}
	rstring AssimpImporter::GetName()
	{
		return rstring();
	}
	rid AssimpImporter::AddObserver(robserver * Observer, rid EventId)
	{
		return rid();
	}
	void AssimpImporter::RemoveObserver(rid ObserverId)
	{
	}
	void * AssimpImporter::GetUserComponentArea(rid userid)
	{
		return nullptr;
	}
	const rchar * AssimpImporter::GetClass()
	{
		return nullptr;
	}
	bool AssimpImporter::IsA(const rchar * type) const
	{
		return AssimpImporter::IsClassA(type);
	}
}